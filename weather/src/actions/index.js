import axios from 'axios'

const API_KEY = '076baf9361ee7df66aadba1d9dbfd1e2'
const ROOT_URL =  `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = 'FETCH_WEATHER'

export function fetchWeather(city) {
    const url = `${ROOT_URL}&q=${city},nz`
    const request = axios.get(url)

    console.log('Request: ', request)

    return {
        type: FETCH_WEATHER,
        payload: request
    }
}