import React from 'react';
import VideoListItem from './video_list_item';

const styles = {
    gridList: {
        width: 500,
        height: 450,
        overflowY: 'auto',
    },
};

const VideoList = (props) => {

    const {classes} = props

    const videoItems = props.videos.map(( video ) => {
        return (
            <VideoListItem
                key={video.etag}
                video={video}
                onVideoSelect={props.onVideoSelect}
                classes={classes}
            />
        )
    })

    return (
        <ul style={styles.gridList}>
            {videoItems}
        </ul>
    )
}

export default VideoList;