import React, { Component } from 'react';
import { Input, Switch, Checkbox } from 'material-ui';

class SearchBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            term: '',
            checked: false,
            toggled:false,
            disabled: false,
        };
    }

    render() {
        const {classes} = this.props
        return (
            <div className={classes.search_bar}>
                <Switch
                    checked={this.state.disabled}
                    className={classes.toggle}
                    onChange={this.onSwitchChange.bind(this)}
                    color='primary'
                />

                <Checkbox
                    checked={this.state.checked}
                    onChange={this.onCheckboxChange.bind(this)}
                    className={classes.checkbox}
                    disabled={this.state.disabled}
                />

                <Input
                    value={this.state.term}
                    onChange={event =>  this.onInputChange(event.target.value)}
                    className={classes.search_bar_input}
                    placeholder="Search"
                    disabled={this.state.disabled}

                />
            </div>
        );
    }

    onInputChange(term) {
        this.setState({ term })
        this.props.onSearchTermChange(term)
    }

    onCheckboxChange() {
        this.setState((oldState) => {
            return {
                checked: !oldState.checked,
            };
        });
    }

    onSwitchChange() {
        this.setState((oldState) => {
            return {
                toggled: !oldState.toggled, disabled: !oldState.toggled
            };
        });
    }
}

export default SearchBar;