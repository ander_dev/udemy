import React from 'react'

const VideoDetail = ({video, classes}) => {

    if(!video) return <div>Loading...</div>

    const videoId = video.id.videoId
    const url = `https://www.youtube.com/embed/${videoId}`

    return (
        <div className="video-detail col-md-8">
            <div className={classes.video_details} color="primary">
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe className="embed-responsive-item" src={url}></iframe>
                </div>
            </div>

            <div className={classes.video_details}>
                <div>{video.snippet.title}</div><br/>
                <div>{video.snippet.description}</div>
            </div>
        </div>
    )
}

export default VideoDetail