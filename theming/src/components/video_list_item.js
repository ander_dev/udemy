import React from 'react'

const VideoListItem = ({video, onVideoSelect, classes}) => {
    const video_detail = video.snippet
    const imageUrl = video_detail.thumbnails.default.url
    return (
        <li className={classes.list_group_item}  onClick={() => onVideoSelect(video)}>
            <div className="video-list media">
                <div className="media-left">
                    <img className="media-object" src={imageUrl}/>
                </div>
                <div className="media-body">
                    <div className="media-heading">
                        {video_detail.title}
                    </div>
                </div>
            </div>
        </li>
    )
}

export default VideoListItem;