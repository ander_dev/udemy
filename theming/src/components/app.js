import React, {Component} from 'react';
import _ from 'lodash'
import YTSearch from 'youtube-api-search'
import SearchBar from './search_bar';
import VideoList from './video_list'
import VideoDetail from './video_detail'
import Button from 'material-ui/Button';
import Dialog, {DialogActions, DialogContent, DialogContentText, DialogTitle,} from 'material-ui/Dialog';
import Typography from 'material-ui/Typography';

const YOUTUBE_API_KEY = 'AIzaSyD378KKrPXZ8EFIh52msSGfCqSvFFtP6I8';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            drawer: false,
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('surfboards')
    }

    videoSearch(term) {
        YTSearch({ key: YOUTUBE_API_KEY, term: term}, (videos) => {
            // console.log(videos);
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            })
        });
    }

    handleClose = () => {
        this.setState({
            ...this.state, open: false
        });
    };

    handleClick = () => {
        this.setState({
            ...this.state, open: true
        });
    };

    render() {

        const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300)
        const {classes} = this.props
        const {open} = this.state;
        console.log(classes)
        return (
            <div>
                <div className={classes.mainContainer}>
                    <Dialog open={open} onClose={this.handleClose}>
                        <DialogTitle>Super Secret Password</DialogTitle>
                        <DialogContent>
                            <DialogContentText>1-2-3-4-5</DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button color="primary" onClick={this.handleClose}>
                                OK
                            </Button>
                        </DialogActions>
                    </Dialog>
                    <Typography variant="display1" gutterBottom>
                        Material-UI
                    </Typography>
                    <Typography variant="subheading" gutterBottom>
                        example project
                    </Typography>
                    <Button variant="raised" color="primary" onClick={this.handleClick}>
                        Super Secret Password
                    </Button>
                </div>

                <div>
                    <SearchBar onSearchTermChange={videoSearch} classes={classes}/>
                    <VideoDetail video={this.state.selectedVideo} classes={classes}/>
                    <VideoList classes={classes}
                        onVideoSelect={selectedVideo => this.setState({ selectedVideo }) }
                        videos={this.state.videos}/>
                </div>
            </div>
        );
    }
}

export default App;
