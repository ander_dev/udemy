import React from 'react';
import App from "../components/app";
import createMuiTheme from "material-ui/es/styles/createMuiTheme";
import MuiThemeProvider from "material-ui/es/styles/MuiThemeProvider";
import PropTypes from 'prop-types';
import {AppBar, Tab, Tabs} from "material-ui";
import fontThem from "../themes/fontTheme";
import purpleThem from "../themes/purpleTheme";
import {CrombieStyles} from "../themes/crombie-styles";

const defaultTheme = createMuiTheme();

const purpleTheme = createMuiTheme(purpleThem);

const fontTheme = createMuiTheme(fontThem)

const themes = [defaultTheme, purpleTheme, fontTheme]

function TabContainer(props) {
    console.log(props)
    return (
        <App style={{ padding: 8 * 3 }} classes={props.classes}/>
    );
}

class Index extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            value: 0,
            theme: defaultTheme
        };
    }

    handleChange = (event, value) => {
        this.setState({
            value, theme:themes[value]
        });
    };

    render() {
        const { classes } = this.props;
        const { value, theme } = this.state;

        return (
            <MuiThemeProvider theme={theme}>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.handleChange}>
                            <Tab label="Default Theme" href="#default-theme"/>
                            <Tab label="Purple Theme" href="#purple-theme"/>
                            <Tab label="Font Theme" href="#font-theme" />
                        </Tabs>
                    </AppBar>
                    {value === 0 && <TabContainer classes={classes} children={'Default'}/>}
                    {value === 1 && <TabContainer classes={classes} children={'Purple'}/>}
                    {value === 2 && <TabContainer classes={classes} children={'Font'}/>}
                </div>
            </MuiThemeProvider>
        );
    }
}

Index.protoTypes = {
    classes: PropTypes.object.isRequired,
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

export default CrombieStyles(Index);
