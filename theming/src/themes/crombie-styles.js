import withStyles from "material-ui/es/styles/withStyles";

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing.unit * 3,
        backgroundColor: theme.palette.background.paper,
    },
    mainContainer: {
        textAlign: 'center',
        paddingTop: 100,
        height: '250px',
        width: '100%'
    },
    search_bar: {
        margin: '20px',
        textAlign: 'center'
    },
    search_bar_input: {
        width: '75%'
    },
    video_item: {
        maxWidth: '64px'
    },
    video_details: {
        marginTop: '10px',
        padding: '10px',
        border: '8px solid',
        borderRadius: '4px',
        width: '60%',
        float: 'left',
        position: 'relative',
    },
    list_group: {
        width: '34%',
        float: 'right',
        position: 'relative',
        minHeight: '1px',
        paddingRight: '15px',
        paddingLeft: '15px'
    },
    list_group_item: {
        cursor: 'pointer',
        padding: '10px 10px 10px 10px'
    },
    list_group_item_hover: {
        backgroundColor: '#eee'
    },
    toggle: {
        marginBottom: 50,
    },
    checkbox: {
        marginBottom: 16,
    },
});

export const CrombieStyles = withStyles(styles)