import purple from "material-ui/es/colors/purple";
import green from "material-ui/es/colors/green";

export default {
    palette: {
        primary: purple,
        secondary: green
    }
};