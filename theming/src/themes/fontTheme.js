import blue from "material-ui/es/colors/blue";
import blueGrey from "material-ui/es/colors/blueGrey";

export default {
    palette: {
        secondary: blue,
        primary: blueGrey,
    },
    typography: {
        fontFamily: ['Courier', 'Helvetica'],
    }
};