# React App example of how to use Theme

## How to have it running

[Clone its repo](https://github.com/AndersonSantos-CL/theming.git):

```bash
https://github.com/AndersonSantos-CL/theming.git
cd theming
```

Install it and run:

```bash
npm install
npm run start
```

## The idea behind the example

[Theming repo](https://github.com/AndersonSantos-CL/theming.git) is the stage area where I am applying learning about material-ui

## Documentation
https://material-ui-next.com/customization/themes/#themes
