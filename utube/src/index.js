import React, { Component} from 'react';
import ReactDOM from 'react-dom';
import _  from 'lodash'
import YTSearch from 'youtube-api-search'
import SearchBar from './components/search_bar';
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'

const YOUTUBE_API_KEY = 'AIzaSyD378KKrPXZ8EFIh52msSGfCqSvFFtP6I8';

//Create a new component, it should produce a HTML
class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('surfboards')
    }

    videoSearch(term) {
        YTSearch({ key: YOUTUBE_API_KEY, term: term}, (videos) => {
            // console.log(videos);
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            })
        });
    }

    render() {

        const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300)
        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch}/>
                <VideoDetail video={this.state.selectedVideo}/>
                <VideoList
                    onVideoSelect={selectedVideo => this.setState({ selectedVideo }) }
                    videos={this.state.videos}/>
            </div>
        );
    }

}

// take this components and add into the page, in the DOM
ReactDOM.render(<App/>, document.querySelector('.container'));
